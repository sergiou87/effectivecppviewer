//
//  ChapterDetailViewController.h
//  EffectiveCppViewer
//
//  Created by Sergio Padrino on 21/01/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ContentViewController.h"

@class ChapterDetailViewController;

@protocol ChapterDetailViewControllerDataSource

-(NSString *)chapterDetailViewController:(ChapterDetailViewController *)sender contentOfChapter:(int)chapter;

@end


@interface ChapterDetailViewController : ContentViewController

@property (nonatomic, weak) id <ChapterDetailViewControllerDataSource> delegate;

@property (nonatomic) int chapter;

@end
