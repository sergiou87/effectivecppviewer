//
//  ItemDetailViewController.h
//  EffectiveCppViewer
//
//  Created by Sergio Padrino on 20/01/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#include "ContentViewController.h"

@class ItemDetailViewController;

@protocol ItemDetailViewControllerDataSource

-(NSString *)itemDetailViewController:(ItemDetailViewController *)sender contentForItem:(int)item ofChapter:(int)chapter;

@end

@interface ItemDetailViewController : ContentViewController

@property (nonatomic, weak) id <ItemDetailViewControllerDataSource> delegate;

@property (nonatomic) int item;
@property (nonatomic) int chapter;

@end
