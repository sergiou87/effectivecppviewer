//
//  ItemTableViewController.m
//  EffectiveCppViewer
//
//  Created by Sergio Padrino on 20/01/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ItemTableViewController.h"
#import "ItemDetailViewController.h"

@interface ItemTableViewController()

@property (weak, nonatomic) IBOutlet UITableView *tableView;

-(void)configureItemDetailViewController:(ItemDetailViewController *)idvc forItemAtIndexPath:(NSIndexPath *)indexPath;

@end

@implementation ItemTableViewController
@synthesize tableView = _tableView;

@synthesize delegate = _delegate;
@synthesize chapter = _chapter;

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)viewDidUnload
{
    [self setTableView:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return YES;//(interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(void)configureItemDetailViewController:(ItemDetailViewController *)idvc forItemAtIndexPath:(NSIndexPath *)indexPath
{
    idvc.delegate = (id <ItemDetailViewControllerDataSource>) self.delegate;
    idvc.chapter = self.chapter;
    idvc.item = indexPath.row;
    
    NSDictionary *itemDictionary = [self.delegate itemTableViewController:self itemNumber:indexPath.row ofChapter:self.chapter];
    
    idvc.title = /*[NSString stringWithFormat:@"Item %d", indexPath.row + 1]; */ [itemDictionary objectForKey:@"title"];
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [self.delegate itemTableViewController:self numberOfItemsForChapter:self.chapter];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Item Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.textLabel.lineBreakMode = UILineBreakModeWordWrap;
        cell.textLabel.numberOfLines = 2;
    }
    
    // Configure the cell...
    NSDictionary *item = [self.delegate itemTableViewController:self itemNumber:indexPath.row ofChapter:self.chapter];
    
    cell.textLabel.text = [item objectForKey:@"title"];
    
    return cell;
}

#pragma mark - Table view delegate

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    //Only for iPad
    if(!self.splitViewController) return;
    
    // load the storyboard by name
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPad" bundle:nil];
    
    ItemDetailViewController *idvc = [self.splitViewController.viewControllers lastObject];
    
    if(![idvc isMemberOfClass:[ItemDetailViewController class]])
    {
        idvc = [storyboard instantiateViewControllerWithIdentifier:@"Item Detail View Controller"];
        
        NSMutableArray* splitViewControllers = [self.splitViewController.viewControllers mutableCopy];
        [splitViewControllers replaceObjectAtIndex:1 withObject:idvc];
        
        self.splitViewController.viewControllers = splitViewControllers;
    }
    
    [self configureItemDetailViewController:idvc forItemAtIndexPath:indexPath];
    
    [idvc reloadContent];
}

#pragma mark - Prepare Segue

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.identifier isEqualToString:@"Show Item Detail"])
    {
        NSIndexPath *indexPath = [self.tableView indexPathForCell:sender];
        
        ItemDetailViewController *idvc = (ItemDetailViewController *) segue.destinationViewController;

        [self configureItemDetailViewController:idvc forItemAtIndexPath:indexPath];
    }
}

@end
