//
//  ItemDetailViewController.m
//  EffectiveCppViewer
//
//  Created by Sergio Padrino on 20/01/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ItemDetailViewController.h"

@interface ItemDetailViewController()

@end

@implementation ItemDetailViewController

@synthesize delegate = _delegate;
@synthesize chapter = _chapter;
@synthesize item = _item;

#pragma mark - View lifecycle

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self reloadContent];
}

- (void)reloadContent
{
    NSString *itemContent = [self.delegate itemDetailViewController:self contentForItem:self.item ofChapter:self.chapter];
    
    [self loadHtmlContent:itemContent];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return YES;//(interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
