//
//  ChapterDetailViewController.m
//  EffectiveCppViewer
//
//  Created by Sergio Padrino on 21/01/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ChapterDetailViewController.h"

@implementation ChapterDetailViewController

@synthesize delegate = _delegate;
@synthesize chapter = _chapter;

#pragma mark - View lifecycle

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self reloadContent];
}

- (void)reloadContent
{
    NSString *chapterContent = [self.delegate chapterDetailViewController:self contentOfChapter:self.chapter];
    
    [self loadHtmlContent:chapterContent];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return YES;//(interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
