//
//  ChapterTableViewController.m
//  EffectiveCppViewer
//
//  Created by Sergio Padrino on 20/01/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ChapterTableViewController.h"
#import "ItemTableViewController.h"
#import "ItemDetailViewController.h"
#import "ChapterDetailViewController.h"

@interface ChapterTableViewController() <ItemTableViewControllerDataSource, ItemDetailViewControllerDataSource, ChapterDetailViewControllerDataSource>

@property (nonatomic, strong) NSDictionary *book;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation ChapterTableViewController

@synthesize book = _book;
@synthesize tableView = _tableView;

-(void)setBook:(NSDictionary *)book
{
    if(_book != book)
    {
        _book = book;
        
        [self.tableView reloadData];
    }
}

-(void)setTableView:(UITableView *)tableView
{
    if(_tableView != tableView)
    {
        _tableView = tableView;
        
        [_tableView reloadData];
    }
}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSString *bookPath = [[NSBundle mainBundle] pathForResource:@"EffectiveCpp" ofType:@"json"];
    
    NSData *bookData = [NSData dataWithContentsOfFile:bookPath];
    
    NSError *error = nil;
    
    NSDictionary *bookDictionary = [NSJSONSerialization JSONObjectWithData:bookData options:0 error:&error];
    
    if(error)
    {
        NSLog(@"Error: %@", error);
    }else
    {
        self.book = [bookDictionary objectForKey:@"book"];
        self.title = [self.book objectForKey:@"title"];
    }


    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)viewDidUnload
{
    [self setTableView:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return YES;//(interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [[self.book objectForKey:@"chapters"] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Chapter Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    // Configure the cell...
    NSArray *chapters = [self.book objectForKey:@"chapters"];
    
    NSDictionary *chapter = [chapters objectAtIndex:indexPath.row];
    
    cell.textLabel.text = [chapter objectForKey:@"title"];
    
    return cell;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     */
}

-(void)tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath
{    
    [self performSegueWithIdentifier:@"Show Chapter Content" sender:[tableView cellForRowAtIndexPath:indexPath]];
}

#pragma mark - Segue

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.identifier isEqualToString:@"Show Chapter Items"])
    {
        NSIndexPath *indexPath = [self.tableView indexPathForCell:sender];
        
        ItemTableViewController *itvc = (ItemTableViewController *) segue.destinationViewController;
        itvc.delegate = self;
        itvc.chapter = indexPath.row;

        /*
        NSArray *chapters = [self.book objectForKey:@"chapters"];
        
        NSDictionary *chapterDictionary = [chapters objectAtIndex:indexPath.row];
         */
        
        itvc.title = [NSString stringWithFormat:@"Chapter %d", indexPath.row + 1]; // [chapterDictionary objectForKey:@"title"];
        
        //iPad version
        if(self.splitViewController)
        {
            // load the storyboard by name
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPad" bundle:nil];
            
            ChapterDetailViewController *cdvc = [self.splitViewController.viewControllers lastObject];
            
            if(![cdvc isMemberOfClass:[ChapterDetailViewController class]])
            {
                cdvc = [storyboard instantiateViewControllerWithIdentifier:@"Chapter Detail View Controller"];
                
                NSMutableArray* splitViewControllers = [self.splitViewController.viewControllers mutableCopy];
                [splitViewControllers replaceObjectAtIndex:1 withObject:cdvc];

                self.splitViewController.viewControllers = splitViewControllers;
            }
            
            cdvc.delegate = self;
            cdvc.chapter = indexPath.row;
            cdvc.title = [NSString stringWithFormat:@"Chapter %d", indexPath.row + 1]; // [chapterDictionary objectForKey:@"title"];
            
            [cdvc reloadContent];
        }
    } else if([segue.identifier isEqualToString:@"Show Chapter Content"])
    {
        
        NSIndexPath *indexPath = [self.tableView indexPathForCell:sender];
        
        ChapterDetailViewController *cdvc = (ChapterDetailViewController *) segue.destinationViewController;
        cdvc.delegate = self;
        cdvc.chapter = indexPath.row;
        
        cdvc.title = [NSString stringWithFormat:@"Chapter %d", indexPath.row + 1]; // [chapterDictionary objectForKey:@"title"];
    }
}

#pragma mark - Item table view delegate

-(NSDictionary *)itemTableViewController:(ItemTableViewController *)sender itemNumber:(int)item ofChapter:(int)chapter
{
    NSArray *chapters = [self.book objectForKey:@"chapters"];
    
    NSDictionary *chapterDictionary = [chapters objectAtIndex:chapter];
    
    NSArray *items = [chapterDictionary objectForKey:@"items"];
    
    return [items objectAtIndex:item];
}

-(int)itemTableViewController:(ItemTableViewController *)sender numberOfItemsForChapter:(int)chapter
{
    NSArray *chapters = [self.book objectForKey:@"chapters"];
    
    NSDictionary *chapterDictionary = [chapters objectAtIndex:chapter];

    NSArray *items = [chapterDictionary objectForKey:@"items"];
    
    return [items count];
}

#pragma mark - Chapter Detail view delegate

-(NSString *)chapterDetailViewController:(ChapterDetailViewController *)sender contentOfChapter:(int)chapter
{
    NSArray *chapters = [self.book objectForKey:@"chapters"];
    
    NSDictionary *chapterDictionary = [chapters objectAtIndex:chapter];

    return [chapterDictionary objectForKey:@"content"];
}

#pragma mark - Item Detail view delegate

-(NSString *)itemDetailViewController:(ItemDetailViewController *)sender contentForItem:(int)item ofChapter:(int)chapter
{
    NSArray *chapters = [self.book objectForKey:@"chapters"];
    
    NSDictionary *chapterDictionary = [chapters objectAtIndex:chapter];
    
    NSArray *items = [chapterDictionary objectForKey:@"items"];

    NSDictionary *itemDictionary = [items objectAtIndex:item];
    
    return [itemDictionary objectForKey:@"content"];
}

@end
