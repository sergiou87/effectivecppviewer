//
//  ContentViewController.h
//  EffectiveCppViewer
//
//  Created by Sergio Padrino on 21/01/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ContentViewController : UIViewController

- (void)reloadContent;
- (void)loadHtmlContent:(NSString *)htmlContent;

@end
