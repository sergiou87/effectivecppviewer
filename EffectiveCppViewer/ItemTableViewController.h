//
//  ItemTableViewController.h
//  EffectiveCppViewer
//
//  Created by Sergio Padrino on 20/01/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ItemTableViewController;

@protocol ItemTableViewControllerDataSource

-(int)itemTableViewController:(ItemTableViewController *)sender numberOfItemsForChapter:(int)chapter;
-(NSDictionary *)itemTableViewController:(ItemTableViewController *)sender itemNumber:(int)item ofChapter:(int)chapter;

@end

@interface ItemTableViewController : UITableViewController

@property (nonatomic, weak) id<ItemTableViewControllerDataSource> delegate;
@property (nonatomic) int chapter;

@end
